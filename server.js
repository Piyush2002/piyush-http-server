const http = require('http')
const fs = require("fs");
const uuid = require('uuid');



function uuidGenerate(){
    let generatedUuid = uuid.v4();
    return(generatedUuid);
}

const server = http.createServer((req,res) => {
  if(req.url == "/html"){

    fs.readFile("./index.html",(err,data) => {
        if(err){
            res.writeHead(500,{'Content-Type': 'text/plain'});
            res.end( "Error : " , err);
        } 
        else {
            res.writeHead(200,{'Content-Type': 'text/html'});
            res.end(data);                
        }
    })
  } 

  else if(req.url === "/json"){
    fs.readFile("./index.json", (err, data) => {
      if (err) {
          res.writeHead(500, { 'Content-Type': 'text/plain' });
          res.end("Error : " + err);
      } 
      else {
          res.writeHead(200, { "Content-Type": "application/json" });
          res.end(data);
      }
    });
  } 

  else if(req.url =="/uuid"){
    const uuidData = uuidGenerate();
    res.writeHead(200,{"Content-Type":"application/json"});
    res.end(uuidData);
  }

  

  else if(req.url.startsWith("/status/")){

      const statusCode = parseInt(req.url.split("/")[2]);
      console.log("Parsed status code:", statusCode);

      if(statusCode >= 100 && statusCode <= 599){

          res.writeHead(statusCode, {"Content-Type":"text/plain"});
          res.end(String(statusCode));
      } 
      else {

          res.writeHead(400,{"Content-Type":"text/plain"});
          res.end("Invalid status code");
      }
  } 
  else if(req.url.startsWith("/delay/")){

      const delay = parseInt(req.url.split("/")[2]);
      console.log("Parsed delay:", delay);
      setTimeout(()=>{

        res.writeHead(200, {"Content-Type" : "text/plain"});
        res.end(`Delayed response by : ${delay} seconds`);

      },delay*1000)
  }
  else{

    res.writeHead(404, {"Content-Type":"text/plain"});
    res.end("Invalid Request");

  }

})

const port = 8000;

server.listen(port , () => {
    console.log(`port running on ${port}`);
})